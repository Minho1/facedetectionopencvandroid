package vn.codegym.facedetection;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public class MainActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private static String TAG = "MainActivity";
    JavaCameraView javaCameraView;
    Mat mRgba;

    static{
        System.loadLibrary("MyOpencvLibs");
    }
    Display display;
    BaseLoaderCallback mLoaderCallBack = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case BaseLoaderCallback.SUCCESS:
                    javaCameraView.enableView();
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        javaCameraView = findViewById(R.id.java_camera_view);
        javaCameraView.setVisibility(View.VISIBLE);
        javaCameraView.setCvCameraViewListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(javaCameraView!= null)
            javaCameraView.disableView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(javaCameraView!= null)
            javaCameraView.disableView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(OpenCVLoader.initDebug()) {
            Log.i(TAG,"OpenCv loaded successfully");
            mLoaderCallBack.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
        else {
            Log.i(TAG, "OpenCv not loaded successfully");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this, mLoaderCallBack);
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(height, width, CvType.CV_8UC4);
    }

    @Override
    public void onCameraViewStopped() {
        mRgba.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
//        mRgba = inputFrame.rgba();
//        display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
//
//        int orientation = display.getRotation();
//
//        // pure portrait
//        if (orientation == Surface.ROTATION_0)
//        {
//            Mat mRgbaT = mRgba.t();
//            Core.flip(mRgba.t(), mRgbaT, 1);
//            Imgproc.resize(mRgbaT, mRgbaT, mRgba.size());
//
//            OpencvNativeClass.faceDetection(mRgbaT.getNativeObjAddr());
//            return mRgbaT;
//        }
//
//        // landscape 90 clockwise
//        else if (orientation == Surface.ROTATION_90)
//        {
//            OpencvNativeClass.faceDetection(mRgba.getNativeObjAddr());
//            return mRgba;
//        }
//
//        // dirty portrait
//        else if (orientation == Surface.ROTATION_180)
//        {
//            Mat mRgbaT = mRgba.t();
//            Core.flip(mRgba.t(), mRgbaT, 0);
//            Imgproc.resize(mRgbaT, mRgbaT, mRgba.size());
//
//            OpencvNativeClass.faceDetection(mRgbaT.getNativeObjAddr());
//            return mRgbaT;
//        }
//
//        // landscape 270 clockwise
//        else if (orientation == Surface.ROTATION_270)
//        {
//            Core.flip(mRgba, mRgba, -1);
//
//            OpencvNativeClass.faceDetection(mRgba.getNativeObjAddr());
//            return mRgba;
//        }
//
//        // return dummy value for compiler's sake
//        return mRgba;
        mRgba = inputFrame.rgba();

        OpencvNativeClass.faceDetection(mRgba.getNativeObjAddr());
        return mRgba;
    }
}
