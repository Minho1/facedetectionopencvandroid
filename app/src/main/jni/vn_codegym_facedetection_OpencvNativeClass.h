/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
#include <opencv2/opencv.hpp>
#include <android/log.h>

#include <iostream>
#include <fstream>
#include <sstream>
/* Header for class vn_codegym_facedetection_OpencvNativeClass */
using namespace cv;
using namespace std;
#ifndef _Included_vn_codegym_facedetection_OpencvNativeClass
#define _Included_vn_codegym_facedetection_OpencvNativeClass
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     vn_codegym_facedetection_OpencvNativeClass
 * Method:    faceDetection
 * Signature: (J)V
 */

void detect(Mat& frame);
JNIEXPORT void JNICALL Java_vn_codegym_facedetection_OpencvNativeClass_faceDetection
  (JNIEnv *, jclass, jlong);

#ifdef __cplusplus
}
#endif
#endif
