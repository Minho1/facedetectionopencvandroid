#include "vn_codegym_facedetection_OpencvNativeClass.h"

JNIEXPORT void JNICALL Java_vn_codegym_facedetection_OpencvNativeClass_faceDetection
(JNIEnv *, jclass, jlong addrRgba) {
    Mat& frame = *(Mat*) addrRgba;
    detect(frame);
}
static void read_csv(const string& filename, vector<Mat>& images, vector<int>& labels, char separator = ';') {
    std::ifstream file(filename.c_str(), ifstream::in);
    if (!file) {
        string error_message = "No valid input file was given, please check the given filename.";
        CV_Error(CV_StsBadArg, error_message);
    }
    string line, path, classlabel;
    while (getline(file, line)) {
        stringstream liness(line);
        getline(liness, path, separator);
        getline(liness, classlabel);
        __android_log_print(ANDROID_LOG_INFO, "path", "path = %s label = %s", path.c_str(), classlabel.c_str());
        if(!path.empty() && !classlabel.empty()) {
            images.push_back(imread(path, CV_LOAD_IMAGE_GRAYSCALE));
            labels.push_back(atoi(classlabel.c_str()));
        }
    }
}
void detect(Mat& frame) {
      vector<Mat> images;
      vector<int> labels;
      try {
              //read_csv("/sdcard/download/minhface.ext", images, labels);
              read_csv("/storage/emulated/0//Download/face/minhface.ext", images, labels);
          } catch (cv::Exception& e) {

              exit(1);
          }
      int im_width = images[0].cols;

      int im_height = images[0].rows;
        __android_log_print(ANDROID_LOG_INFO, "width", "width = %d height = %d", im_width, im_height);
      Ptr<FaceRecognizer> model = createFisherFaceRecognizer();
      model->train(images, labels);

       CascadeClassifier haar_cascade;
       //haar_cascade.load("/sdcard/Download/haarcascade_frontalface_default.xml");
haar_cascade.load("/storage/emulated/0//Download/face/haarcascade_frontalface_default.xml");

       Mat original = frame.clone();

       Mat gray;
       cvtColor(original, gray, CV_BGR2GRAY);

       vector< Rect_<int> > faces;
       haar_cascade.detectMultiScale(gray, faces,1.1, 2, 0|CV_HAAR_SCALE_IMAGE, Size(200, 200));

       for(int i = 0; i < faces.size(); i++) {
           // Process face by face:
           Rect face_i = faces[i];
           // Crop the face from the image. So simple with OpenCV C++:
           Mat face = gray(face_i);

           Mat face_resized;
           cv::resize(face, face_resized, Size(im_width, im_height), 1.0, 1.0, INTER_CUBIC);

           int prediction = model->predict(face_resized);
           __android_log_print(ANDROID_LOG_INFO, "predict", "predict = %d", prediction);

           rectangle(original, face_i, CV_RGB(0, 255,0), 1);
           // Create the text we will annotate the box with:
           string box_text = format("Prediction = %d", prediction);

           int pos_x = std::max(face_i.tl().x - 10, 0);
           int pos_y = std::max(face_i.tl().y - 10, 0);
           putText(original, box_text, Point(pos_x, pos_y), FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0,255,0), 2.0);
       }

}